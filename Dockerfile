FROM node

WORKDIR /app

COPY /package.json
RUN pip install -r package.json

COPY . .

CMD [ "python"]
